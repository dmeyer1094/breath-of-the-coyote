﻿/* Had to move autosave in here to allow for separate animator to prevent overlap with objective animators */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutosaveHandler : MonoBehaviour
{
    public Player player;
    public Animator animator;

    public void AutosaveComplete()
    {
        Debug.Log("Autosave Complete");
        animator.SetBool("Saving", false);
    }
}
