# CSE440-BreathOfTheCoyote
CSE 440 - Fall 2019 Project

1st Place - CSUSB Game Design Competition Fall 2019

Project Lead:
-Daniel Meyer

Programmers:
-Daniel Meyer
-Cameron Maclean
-Benjamin Alexander
-Jose Perez
-Christopher Magnuson

3D Artists
-Daniel Meyer (Terrain, Environment)
-JC Mariscal (Characters)
-Jessica Delker (Environment)

2D Artist
-Abbey Disenhouse (UI)

Musician
-David Amando (Music)


Controls:
W       Walk Forward
A       Walk Left
S       Walk Backward
D       Walk Right
Q       Ground Slam
E       Fireball
Shift   Sprint
Space   Jump

The Goal:
Collect a trophy from each of the 3 trials by completing them.  Upon completing all 3 trials walk back to the village to end the game.

Trial of the Mind:
To complete this trial there are 3 beacons placed around the arena that need to be activated.  These are activated by hitting each one with a fireball.  When activated they will begin to glow with particles and once all 3 are activated then the chest will open up and the player can pick up the trophy.

Trial of Strength:
This is a combat trial where the player must defeat 3 imps in order to proceed.  Only 1 imp will spawn at a time with each consecutive imp being more powerful than the last.  The 3 imps are a basic imp with a melee attack, a mage imp with a fireball attack, and a heavy imp with a melee attack and increased health.  Once all 3 imps are killed the chest will open and the player can pick up the trophy.

Trial of Agility:
This is a platforming trial where the player must jump from platform to platform to reach the top of the puzzle.  Once the player reaches the top ring the chest will open and the player can pick up the trophy.
